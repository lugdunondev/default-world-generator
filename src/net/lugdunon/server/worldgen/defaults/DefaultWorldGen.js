Namespace.declare("net.lugdunon.server.worldgen.defaults");
Namespace.require("net.lugdunon.ui.SimpleKeyValuePropertyEditor");
Namespace.newClass("net.lugdunon.server.worldgen.defaults.DefaultWorldGen","net.lugdunon.server.worldgen.IWorldGen");

////

net.lugdunon.server.worldgen.defaults.DefaultWorldGen.prototype.init=function(initData)
{
	this.callSuper(net.lugdunon.server.worldgen.defaults.DefaultWorldGen,"init",[initData]);

	this.getConfigurableOptions()["world.cfg"].sort(function(a,b){return((a.key>b.key)?(1):((a.key<b.key)?(-1):(0)));});

	this.advanced      =false;
	this.propertyEditor=new net.lugdunon.ui.SimpleKeyValuePropertyEditor().init(
		{
			data        :this.getConfigurableOptions()["world.cfg"],
		    addStyles   :["worldGenProps"],
		    kvWidth     :"274",
			keyFieldType:{
				type           :net.lugdunon.ui.table.cell.content.InlineEditorContentCellRenderer.INPUT_WITH_SELECT,
				selectHandler  :this.handleKeySelectMode,
				selectPopulator:this.handleKeySelectPopulation,
				context        :this
			}
		}
	);
	
	return(this);
};

////

net.lugdunon.server.worldgen.defaults.DefaultWorldGen.prototype.handleKeySelectMode=function(e)
{
	e.data.inputEl.val  ($(this).val());
	e.data.inputEl.focus(             );
};

net.lugdunon.server.worldgen.defaults.DefaultWorldGen.prototype.handleKeySelectPopulation=function(select)
{
	game.getStandaloneRequest(
		"getAvailableWorldConfigProps",
		{},
		function(data)
		{
			select.html("");
			
			if(data)
			{
				for(var i=0;i<data.length;i++)
				{
					select.append($("<option/>").text(data[i].name).val(data[i].key));
				}
			}
		},
		this
	);
};

////

net.lugdunon.server.worldgen.defaults.DefaultWorldGen.prototype.toggleAdvanced=function(btn)
{
	this.advanced=!this.advanced;
	
	if(this.advanced)
	{
		btn.html("Simplified");
		this.renderAdvancedUI();
	}
	else
	{
		btn.html("Advanced");
		this.renderSimplifiedUI();
	}
};

net.lugdunon.server.worldgen.defaults.DefaultWorldGen.prototype.renderAdvancedUI=function()
{
	this.propertyEditor.setContainer(this.parent.getContainer());
	this.propertyEditor.render      ();
	this.parent        .getContainer().find(".tableBody").css("height",(this.parent.getContainer().height()-30)+"px");
};

net.lugdunon.server.worldgen.defaults.DefaultWorldGen.prototype.renderSimplifiedUI=function()
{
	this.parent.getContainer().html(Namespace.requireHTML(this.classId));

	var simpleEd=["name","item.death.penalty","vendor.markup","vendor.repair.discount","vendor.max.buyback"];
	
	for(var i=0;i<this.propertyEditor.getData().length;i++)
	{
		for(var j=0;j<simpleEd.length;j++)
		{
			if(this.propertyEditor.getData()[i].key == simpleEd[j])
			{
				this.parent.getContainer().find("[name='"+simpleEd[j]+"']"           ).val (this.propertyEditor.getData()[i].value);
				this.parent.getContainer().find(".label[for='"+simpleEd[j]+"']"      ).html(this.propertyEditor.getData()[i].ttLabel);
				this.parent.getContainer().find(".description[for='"+simpleEd[j]+"']").html(this.propertyEditor.getData()[i].ttDescription);
				this.parent.getContainer().find("[name='"+simpleEd[j]+"']"           ).on(
					"blur",
					{key:simpleEd[j],context:this},
					function(e)
					{
						e.data.context.setValueFor.call(e.data.context,e.data.key,$(this).val());
					}
				);
				this.parent.getContainer().find("[name='"+simpleEd[j]+"']"           ).on(
					"keydown",
					{key:simpleEd[j],context:this},
					function(e)
					{
						if(e.keyCode == 13)
						{
							e.data.context.setValueFor.call(e.data.context,e.data.key,$(this).val());
						}
					}
				);
				
				break;
			}
		}
	}
};

net.lugdunon.server.worldgen.defaults.DefaultWorldGen.prototype.setValueFor=function(key,value)
{
	for(var i=0;i<this.propertyEditor.getData().length;i++)
	{
		if(this.propertyEditor.getData()[i].key == key)
		{
			this.propertyEditor.getData()[i].value=value;
			break;
		}
	}
};


//////


net.lugdunon.server.worldgen.defaults.DefaultWorldGen.prototype.getName=function()
{
	return("Shadow of Moritasgus");
};

net.lugdunon.server.worldgen.defaults.DefaultWorldGen.prototype.getDescription=function()
{
	return("Creates a new instance of Lugdunon's default campaign 'Shadow of Moritasgus'.");
};

net.lugdunon.server.worldgen.defaults.DefaultWorldGen.prototype.buildCreateUI=function(parent)
{
	this.parent  =parent;
	this.advanced=false;
	this.renderSimplifiedUI();
	this.parent.addAuxButton(
		"advToggle",
		"Advanced",
		this.toggleAdvanced,
		this
	);
};


net.lugdunon.server.worldgen.defaults.DefaultWorldGen.prototype.buildConfigureUI=function(parent)
{
	this.parent  =parent;
	this.advanced=false;
	this.renderSimplifiedUI();
	this.parent.addAuxButton(
		"advToggle",
		"Advanced",
		this.toggleAdvanced,
		this
	);
};

net.lugdunon.server.worldgen.defaults.DefaultWorldGen.prototype.handleCreation=function()
{
	var p      ={};
	var cfg    ={};
	var context=this;
	
	cfg["world.cfg"]={};
	
	this.wait=net.lugdunon.ui.Dialog.waitWithLog("Creating Campaign");
	
	for(var i=0;i<this.propertyEditor.getData().length;i++)
	{
		cfg["world.cfg"][this.propertyEditor.getData()[i].key]=this.propertyEditor.getData()[i].value;
	}

	p["worldGen"]=this.classId;
	p["config"]  =JSON.stringify(cfg);
	
	game.postStandaloneRequest(
		"createWorld",
		p,
		this.worldCreationReturned,
		this
	);
	
	//KICKOFF LOGGING
	this.getProgress=function()
	{
		game.getStandaloneRequest(
			"createWorldProgress",
			{},
			function(data)
			{
				if(data && data.logEntry)
				{
					for(var i=0;i<data.logEntry.length;i++)
					{
						context.wait.appendToLog(data.logEntry[i]);
					}
					
					setTimeout(function(){context.getProgress.call(context);},200);
				}
			},
			context
		);
	};
	
	setTimeout(function(){context.getProgress.call(context);},500);
};

net.lugdunon.server.worldgen.defaults.DefaultWorldGen.prototype.worldCreationReturned=function(data)
{
	this.wait.close();
	
	if(data.status === true)
	{
		game.loadGameState("net.lugdunon.states.list.SinglePlayerOptionsList",null,true);
	}
	else
	{
		net.lugdunon.ui.Dialog.ok("Error!","An error occurred:<br/><br/>"+data.message);
	}
};

net.lugdunon.server.worldgen.defaults.DefaultWorldGen.prototype.handleConfiguration=function()
{
	var cfg={};
	cfg["world.cfg"]={};
	
	this.wait=net.lugdunon.ui.Dialog.wait("Reconfiguring Campaign");
	
	for(var i=0;i<this.propertyEditor.getData().length;i++)
	{
		cfg["world.cfg"][this.propertyEditor.getData()[i].key]=this.propertyEditor.getData()[i].value;
	}

	game.postStandaloneRequest(
		"configureWorld",
		{
			worldGen:this.classId,
			worldId :this.worldId,
			config  :JSON.stringify(cfg)
		},
		this.worldConfigurationReturned,
		this
	);
};

net.lugdunon.server.worldgen.defaults.DefaultWorldGen.prototype.worldConfigurationReturned=function(data)
{
	this.wait.close();
	
	if(data.status === true)
	{
		game.loadGameState("net.lugdunon.states.list.SinglePlayerOptionsList",null,true);
	}
	else
	{
		net.lugdunon.ui.Dialog.ok("Error!","An error occurred:<br/><br/>"+data.message);
	}
};